<?php
class User {
    private $conn;
    private $table_name = "users";

    public $id_user;
    public $user_name;
    public $user_password;
    public $user_level;
    public $user_avatar;
    public $password; //para parar de ficar aparecendo o erro de variável indefinida
    public $username; //para parar de ficar aparecendo o erro de variável indefinida
    public $id; //para parar de ficar aparecendo o erro de variável indefinida

    public function __construct($db) {
        $this->conn = $db;
    }

    public function login($userPassword) {
        $query = "SELECT id_user, user_name, user_password, user_level FROM " . $this->table_name . " WHERE user_name = :user_name LIMIT 0,1";
        $stmt = $this->conn->prepare($query);
        
        // Limpa os dados e os vincula à consulta
        $this->user_name = htmlspecialchars(strip_tags($this->user_name));
        $stmt->bindParam(':user_name', $this->user_name);
        
        $stmt->execute();
        
        if ($stmt->rowCount() > 0) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->id_user = $row['id_user'];
            $this->user_name = $row['user_name'];            
            $this->user_level = $row['user_level'];
    
            // Agora usando $userPassword como a senha pura enviada pelo usuário
            if (password_verify($userPassword, $row['user_password'])) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
    

    // Método para ler todos os usuários
    public function read() {
        $query = "SELECT * FROM " . $this->table_name;
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    public function create($avatarUrl) {
        // Query para inserir um registro com o campo user_avatar
        $query = "INSERT INTO " . $this->table_name . " (user_name, user_password, user_level, user_avatar) VALUES (:user_name, :user_password, :user_level, :user_avatar)";
    
        // Preparar a declaração
        $stmt = $this->conn->prepare($query);
    
        // Limpar os dados
        $this->user_name = htmlspecialchars(strip_tags($this->user_name));
        $this->user_password = htmlspecialchars(strip_tags($this->user_password));
        $this->user_level = htmlspecialchars(strip_tags($this->user_level));
        $user_avatar = $avatarUrl; // estou mandando a url toda caso de alguma coisa eu 
    //$user_avatar = htmlspecialchars(strip_tags($avatarUrl)); // Limpeza do avatar
    //$user_avatar = filter_var($avatarUrl, FILTER_SANITIZE_URL); // Sanitização do avatar URL
    
        // Vincular os valores
        $stmt->bindParam(':user_name', $this->user_name);
        $stmt->bindParam(':user_password', $this->user_password);
        $stmt->bindParam(':user_level', $this->user_level);
        $stmt->bindParam(':user_avatar', $user_avatar); // Vincular o avatar
    
        // Executar a query
        if ($stmt->execute()) {
            return true;
        }
    
        return false;
    }
    

    public function update() {
        // Verifica se o usuário existe
        $query = "SELECT COUNT(*) FROM " . $this->table_name . " WHERE id_user = :id_user";
        $stmt = $this->conn->prepare($query);
        $this->id_user = htmlspecialchars(strip_tags($this->id_user));
        $stmt->bindParam(':id_user', $this->id_user);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $userCount = $row['COUNT(*)'];
    
        if ($userCount == 0) {
            // Usuário não existe
            return false;
        } else {
            // Usuário existe, proceder com a atualização
            // Atualizado para incluir a vírgula antes do WHERE
            $query = "UPDATE " . $this->table_name . " SET user_name = :user_name, user_password = :user_password, user_level = :user_level, user_avatar = :user_avatar WHERE id_user = :id_user";

            $stmt = $this->conn->prepare($query);
    
            // Limpar e vincular os novos valores
            $this->user_name = htmlspecialchars(strip_tags($this->user_name));
            $this->user_password = htmlspecialchars(strip_tags($this->user_password));
            $this->user_level = htmlspecialchars(strip_tags($this->user_level));
            $stmt->bindParam(':id_user', $this->id_user);
            $stmt->bindParam(':user_name', $this->user_name);
            $stmt->bindParam(':user_password', $this->user_password);
            $stmt->bindParam(':user_level', $this->user_level);
            $stmt->bindParam(":user_avatar", $this->user_avatar);
    
            // Executar a query de atualização
            if($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getCurrentAvatarUrl() {
        $query = "SELECT user_avatar FROM " . $this->table_name . " WHERE id_user = ?";
        
        $stmt = $this->conn->prepare($query);
    
        // Este é o ID do usuário que estamos buscando
        $stmt->bindParam(1, $this->id_user);
    
        $stmt->execute();
    
        if($stmt->rowCount() > 0) {
            // Se um usuário com esse ID foi encontrado
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
            // Retorna a URL do avatar
            return $row['user_avatar'];
        }
    
        // Se nenhum usuário foi encontrado, retorna null
        return null;
    }

    public function delete() {
        // Prepara a query de deleção
        $query = "DELETE FROM " . $this->table_name . " WHERE id_user = :id_user";
        $stmt = $this->conn->prepare($query);
    
        // Limpa e vincula o valor de id_user
        $this->id_user = htmlspecialchars(strip_tags($this->id_user));
        $stmt->bindParam(':id_user', $this->id_user);
    
        try {
            // Executa a query de deleção
            if ($stmt->execute()) {
                // Verifica se algum registro foi afetado
                if ($stmt->rowCount() > 0) {
                    return true; // Usuário foi deletado
                } else {
                    return false; // Nenhum usuário foi deletado (possivelmente porque o id_user não existe)
                }
            } else {
                return false; // Falha ao executar a query
            }
        } catch (PDOException $exception) {
            // Poderia logar $exception->getMessage() em um arquivo de log para análise
            return false; // Retorna falso em caso de exceção
        }
    }
    
    
    
    
}
