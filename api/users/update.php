<?php
// Permite métodos específicos
// Adicione estes cabeçalhos para permitir CORS
header('Access-Control-Allow-Origin: *');
// Modifique esta linha para permitir apenas POST
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept, Origin, Authorization');

header("Content-Type: application/json; charset=UTF-8");

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    // Retorna apenas os cabeçalhos para a solicitação preflight e termina a execução
    http_response_code(204); // No Content
    exit;
}

// Alterado para receber post por causa da foto.
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    http_response_code(405); // Método não permitido
    echo json_encode(array("message" => "Método não permitido."));
    exit;
}
require_once __DIR__ . '/../../vendor/autoload.php'; 
// Inclui os arquivos necessários
include_once '../config/database.php';
include_once '../models/user.php';
include_once '../middleware/authMiddleware.php'; // Chamando o middleware

use Aws\S3\S3Client;
use Aws\Exception\AwsException;

// Verifica o token JWT antes de processar a solicitação de atualização
$userData = checkJwtToken(); // Executa o middleware para verificar o token

// Obtém a conexão do banco de dados
$database = new Database();
$db = $database->getConnection();

// Cria uma instância do objeto User
$user = new User($db);

// Recebe os dados enviados via FormData
$id_user = $_POST['id_user'] ?? '';
$user_name = $_POST['user_name'] ?? '';
$user_password = $_POST['user_password'] ?? '';
$user_level = $_POST['user_level'] ?? '';
$user_avatar = $_FILES['user_avatar'] ?? '';

// Verifica se todos os campos foram recebidos
if (!empty($id_user) && !empty($user_name) && !empty($user_password) && !empty($user_level)) {
    // Obtém a URL da foto atual do usuário para exclusão posterior (se necessário)
    $current_avatar_url = ''; // Adicione aqui a lógica para obter a URL atual da foto do usuário no banco de dados

    // Verifica se foi enviado um novo arquivo de avatar
    if (!empty($user_avatar['tmp_name'])) {
        // Remove a foto antiga, se existir
        if (!empty($current_avatar_url)) {
            // Adicione aqui a lógica para excluir a foto antiga (por exemplo, usando AWS SDK)
        }

        // Configurações do DigitalOcean Spaces
        $spaceKey = 'DO006V9RCJTKVWLM2G4M';
        $spaceSecret = 'gmaqJcVzkj4YOikrwhMOxulr7bxKfNSmXB2THcyymJY';
        $spaceRegion = 'sgp1'; // Exemplo: nyc3
        $spaceName = 'sklink-img';

        // Cria o cliente S3 para o DigitalOcean Spaces
        $client = new S3Client([
            'version' => 'latest',
            'region'  => $spaceRegion,
            'endpoint' => "https://{$spaceRegion}.digitaloceanspaces.com",
            'credentials' => [
                'key'    => $spaceKey,
                'secret' => $spaceSecret,
            ],
        ]);

        // Move a nova foto para o armazenamento (DigitalOcean Spaces)
        $avatarFileName = uniqid() . '_' . basename($user_avatar['name']); // Gera um nome de arquivo único
        $avatarFilePath = $user_avatar['tmp_name']; // Caminho temporário do arquivo

        try {
            // Faz o upload do arquivo para o DigitalOcean Spaces
            $result = $client->putObject([
                'Bucket' => $spaceName,
                'Key'    => "avatars/{$avatarFileName}",
                'Body'   => fopen($avatarFilePath, 'r'),
                'ACL'    => 'public-read',
            ]);

            // Obtém a URL pública do avatar
            $new_avatar_url = "https://{$spaceName}.{$spaceRegion}.digitaloceanspaces.com/avatars/{$avatarFileName}";

        } catch (AwsException $e) {
            http_response_code(500);
            echo json_encode(["message" => "Erro ao fazer upload para o DigitalOcean Spaces: " . $e->getMessage()]);
            exit;
        }
    } else {
        // Mantém a URL da foto atual se nenhuma nova foto foi enviada
        $new_avatar_url = $current_avatar_url;
    }

    // Atribui os dados do usuário
    
    $user->id_user = $id_user;
    $user->user_name = $user_name;
    $user->user_password = password_hash($user_password, PASSWORD_DEFAULT); // Usa hashing na senha
    $user->user_level = $user_level;
    $user->user_avatar = $new_avatar_url;

    // Tenta atualizar o usuário
    if ($user->update()) {
        // Define o código de resposta para 200 OK
        http_response_code(200);
        echo json_encode(array("message" => "User was updated."));
    } else {
        // Se não conseguir atualizar, pode ser por não encontrar o usuário com o id fornecido
        http_response_code(404);
        echo json_encode(array("message" => "Unable to update user. User not found."));
    }
} else {
    // Dados incompletos
    http_response_code(400);
    echo json_encode(array("message" => "Unable to update user. Data is incomplete."));
}
?>
