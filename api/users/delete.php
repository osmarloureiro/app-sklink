<?php
// Adicione estes cabeçalhos para permitir CORS
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: DELETE'); // Alterado para DELETE
header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept, Origin, Authorization');

// Verifique se a requisição é do tipo OPTIONS, e se for, apenas envie os cabeçalhos e termine a execução.
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    exit(0);
}

// Verifica se o método é DELETE
if ($_SERVER['REQUEST_METHOD'] != 'DELETE') {
    header('HTTP/1.1 405 Method Not Allowed');
    exit('Apenas requisições DELETE são permitidas.');
}

header("Content-Type: application/json; charset=UTF-8");

// Inclui os arquivos necessários
include_once '../config/database.php';
include_once '../models/user.php';

// Obtém a conexão do banco de dados
$database = new Database();
$db = $database->getConnection();

// Cria uma instância do objeto User
$user = new User($db);

// Recebe os dados enviados via DELETE e decodifica o JSON para um objeto PHP
$data = json_decode(file_get_contents("php://input"));

if (!empty($data->id_user)) {
    // Atribui o ID do usuário para a propriedade id_user do objeto User
    $user->id_user = $data->id_user;

    // Tenta deletar o usuário
    if ($user->delete()) {
        // Define o código de resposta para 200 OK
        http_response_code(200);
        echo json_encode(array("message" => "User was deleted."));
    } else {
        // Define o código de resposta para 503 serviço indisponível
        http_response_code(503);
        echo json_encode(array("message" => "Unable to delete user."));
    }
} else {
    // Dados incompletos
    http_response_code(400);
    echo json_encode(array("message" => "Unable to delete user. Data is incomplete."));
}
