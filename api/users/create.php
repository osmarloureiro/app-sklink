<?php
// Adicione estes cabeçalhos para permitir CORS
header('Access-Control-Allow-Origin: *');
// Modifique esta linha para permitir apenas POST
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept, Origin, Authorization');

require_once __DIR__ . '/../../vendor/autoload.php'; 

use Aws\S3\S3Client;
use Aws\Exception\AwsException;

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('HTTP/1.1 405 Method Not Allowed');
    exit('Apenas requisições POST são permitidas.');
}

// Configurações do DigitalOcean Spaces
$spaceKey = 'DO006V9RCJTKVWLM2G4M';
$spaceSecret = 'gmaqJcVzkj4YOikrwhMOxulr7bxKfNSmXB2THcyymJY';
$spaceRegion = 'sgp1'; // Exemplo: nyc3
$spaceName = 'sklink-img';

// Cria o cliente S3 para o DigitalOcean Spaces
$client = new S3Client([
    'version' => 'latest',
    'region'  => $spaceRegion,
    'endpoint' => "https://{$spaceRegion}.digitaloceanspaces.com",
    'credentials' => [
        'key'    => $spaceKey,
        'secret' => $spaceSecret,
    ],
]);



// Verifique se a requisição é do tipo OPTIONS, e se for, apenas envie os cabeçalhos e termine a execução.
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    // Não precisa enviar mais nada, pois os cabeçalhos já foram enviados.
    exit(0);
}

// Adicione uma verificação para permitir apenas requisições POST, rejeitando outros métodos
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    // Envia uma resposta de status 405 (Method Not Allowed) se o método não for POST
    header('HTTP/1.1 405 Method Not Allowed');
    exit('Apenas requisições POST são permitidas.');
}

header("Content-Type: application/json; charset=UTF-8");

// Inclui os arquivos necessários
include_once '../config/database.php';
include_once '../models/user.php';

// Obtém a conexão do banco de dados
$database = new Database();
$db = $database->getConnection();

// Cria uma instância do objeto User
$user = new User($db);

// Processamento do upload do avatar
if (!empty($_FILES['user_avatar']['name'])) {
    $avatar = $_FILES['user_avatar'];
    $timestamp = time();
    $avatarFileName = $timestamp . '_' . basename($avatar['name']);

    try {
        // Upload da imagem para o DigitalOcean Spaces
        $result = $client->putObject([
            'Bucket' => $spaceName,
            'Key'    => "avatars/{$avatarFileName}",
            'Body'   => fopen($avatar['tmp_name'], 'r'),
            'ACL'    => 'public-read',
        ]);

        // URL pública do avatar
        $avatarUrl = "https://{$spaceName}.{$spaceRegion}.digitaloceanspaces.com/avatars/{$avatarFileName}";

    } catch (AwsException $e) {
        http_response_code(500);
        echo json_encode(["message" => "Erro ao fazer upload para o DigitalOcean Spaces: " . $e->getMessage()]);
        exit;
    }
} else {
    http_response_code(400);
    echo json_encode(["message" => "Avatar não enviado ou erro no arquivo."]);
    exit;
}

// Aqui você processa os outros campos POST e realiza a operação de inserção no banco
// Atribui os dados do usuário e a URL do avatar
if (!empty($_POST['user_name']) && !empty($_POST['user_password']) && !empty($_POST['user_level'])) {
    $user->user_name = $_POST['user_name'];
    $user->user_password = password_hash($_POST['user_password'], PASSWORD_DEFAULT);
    $user->user_level = $_POST['user_level'];
    $user->user_avatar = $avatarUrl; // A URL do avatar carregado   

    if ($user->create($avatarUrl)) {
        http_response_code(201);
        echo json_encode(["message" => "User was created.", "avatarUrl" => $avatarUrl]);
    } else {
        http_response_code(503);
        echo json_encode(["message" => "Unable to create user."]);
    }
} else {
    http_response_code(400);
    echo json_encode(["message" => "Data is incomplete."]);
}
