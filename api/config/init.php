<?php
function loadEnvironmentVariables($path) {
    if (file_exists($path)) {
        // Carrega variáveis de ambiente do arquivo .env
        $lines = file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        foreach ($lines as $line) {
            if (strpos(trim($line), '#') === 0) {
                continue; // Pula comentários
            }
            list($name, $value) = explode('=', $line, 2);
            $name = trim($name);
            $value = trim($value);
            if (!getenv($name)) { // Só define se a variável ainda não estiver no ambiente
                putenv("$name=$value");
            }
        }
    } else {
        // Se o arquivo .env não existir, assuma que as variáveis de ambiente estão definidas no servidor
        // Não é necessário fazer nada aqui, pois o PHP já tem acesso às variáveis de ambiente do sistema
        // Se precisar, você pode adicionar lógica para verificar a existência de variáveis específicas
        // Exemplo:
        // if (!getenv('NOME_VARIAVEL')) {
        //     throw new Exception("A variável de ambiente NOME_VARIAVEL não está definida.");
        // }
    }
}

$envPath = __DIR__ . '/../../.env';
loadEnvironmentVariables($envPath);
