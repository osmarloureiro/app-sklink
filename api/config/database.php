<?php

require_once __DIR__ . '/init.php'; // Se 'init.php' está no mesmo diretório que 'database.php'


class Database {
    public $conn;

    public function getConnection() {
        $this->conn = null;
        
        // Utilizando as variáveis de ambiente definidas
        $host = getenv('DB_HOST');
        $port = getenv('DB_PORT'); // Obtendo a porta do .env
        $db_name = getenv('DB_DATABASE');
        $username = getenv('DB_USERNAME');
        $password = getenv('DB_PASSWORD');
    
        try {
            // Incluindo a porta na string DSN, caso esteja definida
            $dsn = "mysql:host=$host;dbname=$db_name" . ($port ? ";port=$port" : "");
            $this->conn = new PDO($dsn, $username, $password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn->exec("set names utf8");    
           // echo "Conexão com o banco de dados MySQL estabelecida com sucesso!<br>";
        } catch(PDOException $exception) {
            echo "Connection error: " . $exception->getMessage() . "<br>";
        }
    
        return $this->conn;      
    }
}


// Para testar a conexão, instancie a classe Database e chame getConnection
// $dbInstance = new Database();
// $conn = $dbInstance->getConnection();
