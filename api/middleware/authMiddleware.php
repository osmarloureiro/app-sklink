<?php
// middleware.php ou qualquer nome que você escolher para o arquivo

require_once __DIR__ . '/../config/init.php'; 
require_once __DIR__ . '/../config/database.php'; 
require_once __DIR__ . '/../models/user.php'; 
require_once __DIR__ . '/../../vendor/autoload.php'; 

use Firebase\JWT\JWT;
use Firebase\JWT\Key; // Se estiver usando a biblioteca firebase/php-jwt versão 5.0 ou superior
use Exception; // Certifique-se de que esteja usando ou capturando a classe de Exceção correta

function checkJwtToken() {
    $headers = apache_request_headers();
    $authHeader = $headers['Authorization'] ?? null;

    if ($authHeader) {
        list($jwt) = sscanf($authHeader, 'Bearer %s');

        if ($jwt) {
            try {
                $decoded = JWT::decode($jwt, new Key(getenv('JWT_SECRET'), 'HS256'));
                // Token válido, pode prosseguir com a requisição
                // Aqui você pode armazenar os dados do usuário em algum lugar ou retornar o objeto
                return $decoded->data;
            } catch (Exception $e) {
                // Token inválido ou expirado
                http_response_code(401);
                echo json_encode(array("message" => "Acesso negado."));
                exit;
            }
        }
    }

    // Cabeçalho de autorização ausente ou malformado
    http_response_code(401);
    echo json_encode(array("message" => "Token não fornecido ou malformado."));
    exit;
}

// E no início de cada arquivo de rota que requer proteção, você incluiria:
// checkJwtToken();
