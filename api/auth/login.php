<?php
require_once __DIR__ . '/../config/init.php'; 
require_once __DIR__ . '/../config/database.php'; 
require_once __DIR__ . '/../models/user.php'; 
require_once __DIR__ . '/../../vendor/autoload.php'; 

use \Firebase\JWT\JWT;

// Cabeçalhos HTTP para uma API
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST, OPTIONS");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    // Termina a execução do script enviando cabeçalhos para a requisição pre-flight
    http_response_code(200);
    exit;
}

// Obtém a conexão do banco de dados
$database = new Database();
$db = $database->getConnection();

// Prepara o objeto User
$user = new User($db);

// Obtém os dados postados
$data = json_decode(file_get_contents("php://input"));

if (!empty($data->user_name) && !empty($data->user_password)) {
    // Define o nome de usuário
    $user->user_name = $data->user_name;

    // Tenta fazer login com a senha fornecida
    if ($user->login($data->user_password)) { // Ajustado para passar a senha como argumento
        $user_level = $user->user_level;
        $token = array(
            "iat" => time(), // Emitido em
            "exp" => time() + 3600, // Expiração (exemplo: 1 hora após a emissão)
            "data" => array(
                "id_user" => $user->id_user,
                "user_name" => $user->user_name,
                "user_level" => $user_level // Passa o user_level para o front-end
               
            )
        );

        $jwtSecretKey = getenv('JWT_SECRET'); // Certifique-se de que essa chave está definida
        $jwt = JWT::encode($token, $jwtSecretKey, 'HS256');
        http_response_code(200);
        echo json_encode(
            array(
                "message" => "Login bem-sucedido.",
                "jwt" => $jwt,
                "user_level" => $user_level // Incluído no JSON de resposta
            )
        );
    } else {
        // Se o login falhar, retorne 401
        http_response_code(401);
        echo json_encode(array("message" => "Login falhou. Verifique o nome de usuário/senha."));
    }
} else {
    // Se dados incompletos, retorne 400
    http_response_code(400);
    echo json_encode(array("message" => "Dados incompletos."));
}
