# API de Gerenciamento de Usuários

Bem-vindo ao repositório da nossa API de Gerenciamento de Usuários, onde a simplicidade encontra a funcionalidade. Este projeto é liderado pelo Gerente de Projetos Antonio Loureiro, com contribuições do desenvolvedor Djun Tsukamoto. Nossa API oferece uma solução ágil e escalável para o gerenciamento de usuários, com foco em operações CRUD e autenticação segura.

## Começando

Essas instruções fornecerão uma cópia do projeto em execução na sua máquina local para fins de desenvolvimento e teste. Veja as notas de instalação sobre como implantar o projeto em um sistema ao vivo.

### Pré-requisitos

O que você precisa para instalar o software:

- PHP 7.4+
- Composer
- SQLite

### Instalação

Clone o repositório:

```bash
git clone https://gitlab.com/osmarloureiro/app-sklink.git


# API de Gerenciamento de Usuários

Bem-vindo ao repositório da nossa API de Gerenciamento de Usuários, onde a simplicidade encontra a funcionalidade. Este projeto é liderado pelo Gerente de Projetos Antonio Loureiro, com contribuições do desenvolvedor Djun Tsukamoto. Nossa API oferece uma solução ágil e escalável para o gerenciamento de usuários, com foco em operações CRUD e autenticação segura.

## Começando

Essas instruções fornecerão uma cópia do projeto em execução na sua máquina local para fins de desenvolvimento e teste. Veja as notas de instalação sobre como implantar o projeto em um sistema ao vivo.

### Pré-requisitos

O que você precisa para instalar o software:

- PHP 7.4+
- Composer
- SQLite

### Instalação

Clone o repositório:

```bash
git clone https://gitlab.com/osmarloureiro/app-sklink.git

### Instale as dependências do PHP com o Composer:

```composer install 

### Inicie o servidor embutido do PHP:

```php -S localhost:8000

Utilização
Com o servidor em execução, a API está pronta para receber solicitações para criar, ler, atualizar e deletar usuários.

Características
Autenticação JWT: Segurança e simplicidade com tokens JWT.
CRUD de Usuários: Gerencie usuários com operações de criar, ler, atualizar e deletar.
Validação: Entradas de usuários são validadas para garantir dados confiáveis.
SQLite Database: Utiliza SQLite para fácil configuração e manutenção.
PHP 7.4+: Escrito na versão moderna do PHP para melhor desempenho e segurança.
Contribuições
Contribuições são o que fazem a comunidade open source um lugar incrível para aprender, inspirar e criar. Quaisquer contribuições que você fizer serão muito apreciadas.

Faça um Fork do projeto
Crie sua Feature Branch (git checkout -b feature/AmazingFeature)
Faça commit de suas mudanças (git commit -m 'Add some AmazingFeature')
Faça Push para a Branch (git push origin feature/AmazingFeature)
Abra um Pull Request
Licença
Distribuído sob a Licença MIT. Veja LICENSE para mais informações.

Contato
Antonio Loureiro - antonio@ninescgroup.com
Djun Tsukamoto - djun@ninescgroup.com

Link do Projeto: https://gitlab.com/osmarloureiro/app-sklink