Contribuindo para app-sklink
Primeiramente, obrigado por considerar contribuir para [app-sklink]! É por causa de pessoas como você que a comunidade open-source prospera.

Código de Conduta
[app-sklink] adota um código de conduta que todos os contribuidores devem seguir. Leia o arquivo CODE_OF_CONDUCT.md antes de contribuir.

Como Contribuir
Para contribuir para [app-sklink], siga estes passos:

Fork o repositório: Crie sua própria cópia do repositório para trabalhar independentemente.

Clone o repositório: Faça o download do seu fork no seu ambiente de desenvolvimento local.

Crie uma branch: Se estiver trabalhando em uma nova funcionalidade ou correção, crie uma branch a partir da branch main.

Faça suas alterações: Adicione suas novas funcionalidades ou correções.

Escreva testes: Para garantir que sua funcionalidade ou correção funcione como esperado.

Documente suas mudanças: Atualize a documentação se estiver introduzindo alterações que precisem de explicação ou uso.

Envie um Pull Request: Quando estiver satisfeito com as alterações, envie um pull request para o repositório original para revisão.

Utilizando Issues
Use issues para relatar problemas, pedir funcionalidades ou discutir ideias para o projeto.
Pull Request Processo
Atualize a documentação com as mudanças correspondentes, se houver.
Aumente os números de versão em qualquer arquivo de exemplo e o README, se você propôs mudanças a esse respeito.
O pull request será avaliado pelos mantenedores e, se aprovado, será mesclado ao projeto.
Estilo de Código & Convenções
Detalhe quaisquer regras específicas sobre codificação, estilo de código, ou convenções que os contribuidores devem seguir.

Agradecemos a sua contribuição!